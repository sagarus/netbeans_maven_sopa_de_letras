/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;
import Negocio.SopaDeLetras;
import java.io.FileInputStream;
import org.apache.poi.hssf.usermodel.*;



 

/**
 *
 * @author madarme
 */
public class Matriz_Excel_Ejemplo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        SopaDeLetras s = leerExcel();
        
        try
        {        
            System.out.println(s.toString());
            //System.out.println(s.toString2());

            if(s.esCuadrada()==true)
            {
                System.out.println("es cuadrada");
            }
            if(s.esDispersa()==true)
            {
                System.out.println("es dispersa");
            }
            if(s.esRectangular()==true)
            {
                System.out.println("es rectangular");
            }     
            System.out.println(s.getDiagonalPrincipal());          
            
            System.out.println(s.getContar("t u"));

            System.out.println("*******" + "\n");            
        }catch(Exception error){

            System.err.println(error.getMessage());

        }
        
    }
     public static SopaDeLetras leerExcel() throws Exception {
        SopaDeLetras s = new SopaDeLetras();
        HSSFWorkbook archivoExcel = new HSSFWorkbook(new FileInputStream("Sopa De Letras.xls"));
        //Obtiene la hoja 1
        HSSFSheet hoja = archivoExcel.getSheetAt(0);
        //Obtiene el número de la última fila con datos de la hoja.
        int canFilas = hoja.getLastRowNum()+1;
        s.asignarCantFilas(canFilas);
        int cantCol = 0;
       
        for (int i = 0; i < canFilas; i++) {
            //Obtiene el índice de la última celda contenida en esta fila MÁS UNO.
            HSSFRow filas = hoja.getRow(i);
            //Obtiene la cantidad de colomunas de esa fila
            cantCol=filas.getLastCellNum();
            s.asignarCantColumnas(i,cantCol);
            
            for(int j=0;j<cantCol;j++)    
            {
                //Obtiene la celda y su valor respectivo
                //double r=filas.getCell(i).getNumericCellValue();
                String valor=filas.getCell(j).getStringCellValue();
                char valorChar = valor.charAt(0);
                s.setValorMatriz(i,j,valorChar);
                
            }
  
       }
        s.getContar("NO");
        return s;
    }  
     

}