package Negocio;

/**
 * Write a description of class SopaDeLetras here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class SopaDeLetras
{
    // instance variables - replace the example below with your own
    private char sopas[][];

    /**
     * Constructor for objects of class SopaDeLetras
     */
    public SopaDeLetras()
    {
        
    }
    
    public void asignarCantFilas (int filas)
    {
        this.sopas = new char [filas][];
    }
    
    public void asignarCantColumnas (int fila,int columnas)
    {
        this.sopas[fila] = new char [columnas];
    }
    
    
    
    public String toString()
    {
    String msg="";
    for(int i=0;i<this.sopas.length;i++)
    {
        for (int j=0;j<this.sopas[i].length;j++)
        {
            msg+=this.sopas[i][j]+"\t";
        }
        
        msg+="\n";
        
    }
    return msg;
    }
    
    
    public String toString2(char [] arreglo)
    {
    String msg="";
    for(int i=0;i<arreglo.length;i++)
    {
         msg+=arreglo[i]+"\t";       
    }
    return msg;
    }
   
    
    public boolean esCuadrada()
    {
        boolean esCuadrada = true;
        for(int i = 0; i<sopas.length && esCuadrada==true;i++)
        {
            esCuadrada = sopas.length == sopas[i].length; 
        }      
        return esCuadrada;
    }
    
    
     public boolean esDispersa()
    {
        boolean esDispersa = false;
        int cantidadColumnasFila0 = this.sopas[0].length;

        for(int i = 1; i<sopas.length && esDispersa==false;i++)
        {
            esDispersa = cantidadColumnasFila0 != this.sopas[i].length;
        }        
        return esDispersa;

    }
    
    public boolean esRectangular()
    {
        boolean esRectangular = false;        
        esRectangular = esCuadrada() == false && esDispersa() == false;      
        return esRectangular;

    }

    /*
        retorna cuantas veces esta la palabra en la matriz desde una letra como punto de
       */
    public int getContar(String palabra)
    {
        int contador=0;
        int k=0;
        String palabrasSinEspacios = palabra.replace(" ","");
        char buscar[]= new char[palabrasSinEspacios.length()];

        for(int i=0;i<palabrasSinEspacios.length();i++)
        {
            buscar[i]=palabrasSinEspacios.charAt(i);
        }

        for(int i=0;i<sopas.length;i++)
        {        
            for(int j=0;j<sopas[i].length;j++){
                if(this.sopas[i][j]==buscar[0]){
               if(i+buscar.length-1 < sopas.length && j+buscar.length-1 < sopas[i].length){
               buscadorTodoSentido(i,j,1,1,buscar);
               }
               if(i+buscar.length-1 < sopas.length){
               buscadorTodoSentido(i,j,1,0,buscar);
               }
               if(i+buscar.length-1 < sopas.length && j-(buscar.length-1) >= 0){
               buscadorTodoSentido(i,j,1,-1,buscar);
               }
               if(i-(buscar.length-1) >= 0){
               buscadorTodoSentido(i,j,-1,0,buscar);
               }
               if(j-(buscar.length-1) >= 0){
               buscadorTodoSentido(i,j,0,-1,buscar);
               }
               if(i-(buscar.length-1) >= 0 && j+buscar.length-1 < sopas[i].length){
               buscadorTodoSentido(i,j,-1,1,buscar);
               }
               if(j+buscar.length-1 < sopas[i].length){
               buscadorTodoSentido(i,j,0,1,buscar);
               }
               if(i-(buscar.length-1) >= 0 && j-(buscar.length-1) >= 0){
               buscadorTodoSentido(i,j,-1,-1,buscar);
               }
                }
            }  
            
        }
        return contador;
    }
    
      public int buscadorTodoSentido(int y, int x, int direccionFila,int direccionColumna,char[] palabra){
        int i=1;
        int k=1;
        int contador=0;
        String direccionX=" ";
        String direccionY=" ";
        String diagonal=" ";
        
        if(direccionFila==1)
            direccionY="abajo";
        if(direccionFila==-1)
            direccionY="arriba";
        if(direccionColumna==1)
            direccionX="derecha";
        if(direccionColumna==-1)
            direccionX="izquierda";
        if(direccionColumna!=0&&direccionFila!=0)
            diagonal="diagonal";
            
        for(int j=1;j<palabra.length;j++){

            if(this.sopas[y+(i*direccionFila)][x+(j*direccionColumna)]==palabra[i]){
                k++;

            }else{
                k = 1;
                if(this.sopas[y+(i*direccionFila)][x+(j*direccionColumna)]==palabra[k]){
                    k++;
                }                              
            }
            if(k==palabra.length){
                contador++; 
                k=1;
                System.out.println("inicio"+x+","+y+"final "+diagonal+" "+ direccionX +" "+direccionY);
            }  
            i++;
            }
        return contador;
    }
    
    /*
        debe ser cuadrada sopas
       */
    public char[] getDiagonalPrincipal() throws Exception
    { 
        if(esCuadrada() == false)
        {
            throw new Exception ("la matriz no es cuadra. por lo tanto, no tiene diagonal principal");
        }
        //comenzar a sacar la diagonal         
        char diagonal [] = new char [sopas.length];

        for(int i = 0; i<diagonal.length;i++)
        {
            diagonal[i] = sopas[i][i];
        }
        
        return diagonal;

    }

    /*GET Method Propertie sopas*/
    public char[][] getSopas(){
        return this.sopas;
    }//end method getSopas

    public void setValorMatriz(int fila,int columna,char valor)
    {
        this.sopas[fila][columna] = valor;
    }
    }